package com.devcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.model.CCountry;
import com.devcamp.repository.CountryRepository;

@RestController
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@GetMapping("/country/{countryName}/{start}/{end}")
		public ResponseEntity<List<CCountry>> findCountryByCountryName(@PathVariable String countryName,@PathVariable int start,@PathVariable int end) {
			try {
				List<CCountry> vCountry = countryRepository.findCountryByCountryNameDesc(countryName,PageRequest.of(start, end));
				return new ResponseEntity<>(vCountry, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);	
		}
	}
}
