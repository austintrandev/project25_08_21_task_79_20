package com.devcamp.repository;

import com.devcamp.model.CCountry;

import java.util.List;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
	@Query(value = "SELECT * FROM country  WHERE country_name like %:name% ORDER BY country_name DESC", nativeQuery = true)
	List<CCountry> findCountryByCountryNameDesc(@Param("name") String name,Pageable pageable);

}
